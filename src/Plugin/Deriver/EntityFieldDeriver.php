<?php

namespace Drupal\context_entity_field\Plugin\Deriver;

use Drupal\Core\Entity\Plugin\Condition\Deriver\EntityBundle;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Deriver that creates a condition for each entity type with bundles.
 */
class EntityFieldDeriver extends EntityBundle {

  use StringTranslationTrait;

  /**
   * Provides the bundle label with a fallback when not defined.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type we are looking the bundle label for.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The entity bundle label or a fallback label.
   */
  protected function getEntityBundleLabel($entity_type) {

    if ($label = $entity_type->getBundleLabel()) {
      return $this->t('@label field', ['@label' => $label]);
    }

    $fallback = $entity_type->getLabel();
    if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
      // This is a better fallback.
      $fallback = $this->entityTypeManager->getDefinition($bundle_entity_type)->getLabel();
    }

    return $this->t('@label bundle field', ['@label' => $fallback]);

  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->hasKey('bundle')) {
        $this->derivatives[$entity_type_id] = $base_plugin_definition;
        $this->derivatives[$entity_type_id]['label'] = $this->getEntityBundleLabel($entity_type);
        $this->derivatives[$entity_type_id]['provider'] = $entity_type->getProvider();
        $this->derivatives[$entity_type_id]['context_definitions'] = [
          $entity_type_id => EntityContextDefinition::fromEntityType($entity_type),
        ];
      }
    }
    return $this->derivatives;
  }

}
